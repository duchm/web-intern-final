# Final Project
- **Tên bài:** Website truyện tranh - Magakool
- **Người soạn:** DucHM (18/01/2022)
- **Người hướng dẫn:** DucHM
- **Ngày tiến hành:** 19/01/2022
- **Ngày báo cáo tiến độ:** 24/01/2022
- **Hạn thu bài:** 23h59’ ngày 28/01/2022

# Đặc tả yêu cầu bài tập
Xây dựng 1 website để đọc và upload truyện tranh 

## Về kiến thức
- Xây dựng website bằng Nuxt.js
- Sử dụng các thư viện UI như Bootstrap, Element UI và các thư viện khác nếu cần thiết
- Responsive design
- Upload ảnh lên [vgy.me](https://vgy.me/)
- Sử dụng Axios để gửi REST API
- JWT và Authorization Header khi gửi REST request
- Đọc hiểu API Doc trên Swagger Editor
- Sử dụng F12 để inspect request

## Đề bài
Xây dựng website có giao diện và các chức năng giống với trang [Mangakool](https://mangakool.herokuapp.com/).  Cụ thể các chức năng như sau:
- Có 3 loại người dùng: Anonymous, User và Admin:
-- Anonymous là người dùng không đăng nhập, có thể đọc truyện,  tìm kiếm truyện theo tên hoặc thể loại.
-- User là người dùng đã đăng nhập, ngoài các quyền ở trên thì có thể subscibe truyện để tiện truy cập, comment vào cuối mỗi chap, đổi mật khẩu của mình.
-- Admin chỉ có 1 số tài khoản nhất định (khi làm  bài thì sử dụng tài khoản duchm/12345678), ngoài các quyền trên có thể xoá tài khoản khác, thêm/sửa/xoá truyện, thêm/sửa/xoá chapter trong chuyện
- Tất cả ảnh upload lên vgy.me, tạo tài khoản và tạo user  key để sử dụng
- Lưu ý khi thêm/sửa chapter:
-- Phần content là 1 chuỗi các url của ảnh cách nhau bằng dấu phẩy hoặc xuống dòng (xem ví dụ ở bất kì truyện nào có sẵn)
-- Có thể điền trực tiếp url ngoài vào
-- Nếu sử dụng nút upload thì có thể  chọn upload được nhiều ảnh từ máy, sau đó các url sẽ tự xuất hiện ở ô bên dưới để copy và paste vào content (có thể tự cải tiến bằng cách append luôn vào content có sẵn)

## Tài liệu
- Google
- [vgy.me API](https://vgy.me/api)
- Website mẫu [Mangakool](https://mangakool.herokuapp.com/)
- [Swagger Editor](https://editor.swagger.io/)
- Doc API đính kèm

## Cách nộp bài

### 1. Sử dụng GitHub để lưu trữ code
- Tạo project trên GitHub, public hoặc private (nếu không thích public thì add [duchunter](https://github.com/duchunter) vào project) 


### 2. Yêu cầu về kết quả bài tập:
- Mã nguồn chạy được
- Đầy đủ các chức năng đã yêu cầu
- Responsive
- Đẹp bằng hoặc hơn bản gốc =))

### 3. Thông báo hoàn thành bài tập
Báo cáo tiến độ, hông báo hoàn thành bài tập và trao đổi với người hướng dẫn **Hà Minh Đức**  trực tiếp qua Slack, nếu sau 22h Slack không noti thì gửi email đến địa chỉ `duchm@cystack.net` với tiêu đề ``WebProject-<Hoten>``. Phần nội dung thư không để trống, có ghi một số thông tin vắn tắt liên quan. Ví dụ:

```
Chào anh,

Em là xxx, 
    
Project đã được em push lên GitHub tại địa chỉ https://......
    
Rất mong nhận được sự phản hồi từ anh.
    
Em cảm ơn.
```
    
### 4. Lưu ý
- Bài bị phát hiện copy từ người khác, không hiểu nội dung thì sinh viên sẽ cạch
- Bài nộp không theo chuẩn nêu trên thì cũng không được công nhận.
